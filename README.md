# Introduction
This docker container provides a Confluence installation. As far as possible it's configurable through environment variables. The initialization and setup scripts are largely inspired by the superb gitlab container from sameersbn.

# Versions
## Confluence
Current Confluence version: 6.8.3

## MySQL Driver
Current MySQL Driver Version: 5.1.46

